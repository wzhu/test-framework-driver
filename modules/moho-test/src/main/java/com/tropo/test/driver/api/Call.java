package com.tropo.test.driver.api;

import java.util.Map;

import javax.servlet.sip.SipServletRequest;

public interface Call {

  public enum State {
    NEW, INVITING, RINGING, ANSWERED, CANCELED, FAILED, REJECTED, DISCONNECTED
  }

  String getID();

  State getState();

  String getCaller();

  String getCallee();

  byte[] getLocalSDP();

  byte[] getRemoteSDP();

  void say(String text);

  void sayDTMF(String dtmf);

  String ask(String grammar);

  String ask(String grammar, long timeout);

  String ask(String grammar, long timeout, Runnable run);

  /**
   * send update after answer or during earlymedia
   */
  void sendUpdate(Map<String, String> headers);

  void reInvite(Map<String, String> headers);

  void hangup(Map<String, String> headers);

  void waitForHangup();

  SipServletRequest getReceivedReInvite();

  SipServletRequest getOriginalRequest();
}
