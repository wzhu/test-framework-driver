package com.tropo.test.driver.impl;

import java.util.Map;

import javax.servlet.sip.SipServletRequest;
import javax.servlet.sip.SipServletResponse;

import org.apache.log4j.Logger;

import com.tropo.test.driver.api.IncomingCall;

public class IncomingCallImpl extends CallImpl implements IncomingCall {

  private static final Logger LOG = Logger.getLogger(IncomingCallImpl.class);

  public IncomingCallImpl(SipServletRequest invite, CallManagerImpl callManager) {
    super(callManager);
    setOriginalRequest(invite);
    setState(State.INVITING);
    caller = extractUser(invite.getFrom().getURI());
    callee = extractUser(invite.getTo().getURI());
  }

  @Override
  public synchronized void sendProvisiningResponse(int code, boolean withSDP, boolean isReliable,
      Map<String, String> headers) {
    this.sendProvisiningResponse(code, withSDP, isReliable, headers, null, null);
  }

  @Override
  public synchronized void sendProvisiningResponse(int code, boolean withSDP, boolean isReliable,
      Map<String, String> headers, byte[] content, String contentType) {
    sendProvisiningResponse(code, withSDP, isReliable, headers, null, null, false);
  }

  public synchronized void sendProvisiningResponse(int code, boolean withSDP, boolean isReliable,
      Map<String, String> headers, byte[] content, String contentType, boolean createNewNC) {
    if (getState() == State.RINGING || getState() == State.INVITING) {
      try {
        SipServletResponse resp = getOriginalRequest().createResponse(code);
        if (withSDP) {
          if (createNewNC) {
            processSDP(getOriginalRequest(), createNewNC);
          }
          else if (getLocalSDP() == null) {
            processSDP(getOriginalRequest());
          }

          SIPHelper.setContent(resp, getLocalSDP(), null);
        }
        else if (content != null) {
          SIPHelper.setContent(resp, content, contentType);
        }
        SIPHelper.addHeaders(resp, headers);

        if (isReliable) {
          resp.sendReliably();
          waitForPrack();
        }
        else {
          resp.send();
        }
      }
      catch (Exception ex) {
        LOG.error("Exception when sending response. " + this, ex);
        throw new RuntimeException(ex);
      }

      setState(State.RINGING);
    }
    else {
      LOG.warn("Wrong state when sending provisioning response " + this);
    }
  }

  @Override
  public void acceptEarlyMedia(int code, Map<String, String> headers) {
    this.sendProvisiningResponse(183, true, true, null, null, null);
  }

  @Override
  public synchronized void answer(Map<String, String> headers) {
    if (getState() != State.RINGING && getState() != State.INVITING) {
      throw new IllegalStateException(this + " is in wrong state, can't answer");
    }
    try {
      SipServletResponse resp = getOriginalRequest().createResponse(200);

      if (headers != null)
        SIPHelper.addHeaders(resp, headers);

      if (!isInEarlyMedia()) {
        byte[] sdp = processSDP(getOriginalRequest());
        if (this.getSdpProcessState() == SDPState.ProcessedSDPOfer
            || this.getSdpProcessState() == SDPState.GeneratedSDPOffer) {
          SIPHelper.setContent(resp, sdp, null);
        }
      }
      resp.send();
    }
    catch (Exception e) {
      LOG.error("Exception when answering call " + this, e);
      this.setState(State.FAILED);
      throw new RuntimeException("Exception when answering call " + this);
    }

    while (!isDisconnected() && !isAnswered()) {
      try {
        this.wait();
      }
      catch (InterruptedException e) {

      }
    }
  }

  @Override
  public synchronized void reject(int code, Map<String, String> headers) {
    if (getState() == State.RINGING || getState() == State.INVITING) {
      LOG.debug("Rejecting incoming call " + this);
      try {
        SipServletResponse resp = getOriginalRequest().createResponse(code);
        SIPHelper.addHeaders(resp, headers);
        resp.send();
        setState(State.REJECTED);
      }
      catch (Exception e) {
        LOG.error("Exception when rejecting. " + this, e);
        setState(State.FAILED);
        throw new RuntimeException(e);
      }
    }
    else {
      LOG.warn("Wrong state when rejecting. " + this);
    }
  }

  @Override
  public synchronized void hangup(Map<String, String> headers) {
    if (getState() == State.RINGING || getState() == State.INVITING) {
      reject(SipServletResponse.SC_DECLINE, headers);
    }
    else {
      super.hangup(headers);
    }
  }
  
  
}
