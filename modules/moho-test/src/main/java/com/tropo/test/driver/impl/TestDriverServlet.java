package com.tropo.test.driver.impl;

import java.io.IOException;
import java.util.concurrent.Executor;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.media.mscontrol.MsControlFactory;
import javax.media.mscontrol.spi.DriverManager;
import javax.sdp.SdpFactory;
import javax.servlet.ServletException;
import javax.servlet.sip.SipFactory;
import javax.servlet.sip.SipServlet;
import javax.servlet.sip.SipServletRequest;
import javax.servlet.sip.SipServletResponse;

import org.apache.log4j.Logger;
import org.apache.logging.log4j.core.async.DaemonThreadFactory;

import com.tropo.test.driver.Locator;

@javax.servlet.sip.annotation.SipServlet
public class TestDriverServlet extends SipServlet {
  private static final Logger LOG = Logger.getLogger(TestDriverServlet.class);

  private static final long serialVersionUID = -8151421978326502934L;

  protected SipFactory _sipFactory;

  protected SdpFactory _sdpFactory;

  protected MsControlFactory _msControlFactory;

  protected CallManagerImpl _callManager;

  protected Executor _executor = new ThreadPoolExecutor(10, 10, 60, TimeUnit.SECONDS, new SynchronousQueue<Runnable>(),
      new DaemonThreadFactory("MohoFunctionalTestDriver"));

  @Override
  public void init() throws ServletException {
    _sipFactory = (SipFactory) getServletContext().getAttribute(SipServlet.SIP_FACTORY);
    _sdpFactory = (SdpFactory) getServletContext().getAttribute("javax.servlet.sdp.SdpFactory");
    try {
      _msControlFactory = DriverManager.getDrivers().next().getFactory(null);
    }
    catch (final Exception e) {
      throw new ServletException(e);
    }

    _callManager = new CallManagerImpl(_sipFactory, _sdpFactory, _msControlFactory, this);
    Locator.setCallManager(_callManager);
    Locator.setDriverServlet(this);
  }

  @Override
  protected void doInvite(final SipServletRequest invite) throws ServletException, IOException {
    if (invite.isInitial()) {
      new IncomingCallImpl(invite, _callManager);
    }
    else {
      final CallImpl call = Utils.getCall(invite.getSession());
      _executor.execute(new Runnable() {
        @Override
        public void run() {
          call.doReInvite(invite);
        }
      });

    }
  }

  @Override
  protected void doAck(SipServletRequest message) throws ServletException, IOException {
    CallImpl call = Utils.getCall(message.getSession());
    call.doAck(message);
  }

  @Override
  protected void doBye(SipServletRequest message) throws ServletException, IOException {
    CallImpl call = Utils.getCall(message.getSession());
    call.doBye(message);
  }

  @Override
  protected void doCancel(SipServletRequest message) throws ServletException, IOException {
    ((CallImpl) _callManager.getCallBySipSession(message.getSession())).doCancel(message);
  }

  @Override
  protected void doPrack(SipServletRequest message) throws ServletException, IOException {
    CallImpl call = Utils.getCall(message.getSession());
    call.doPrack(message);
  }

  @Override
  protected void doResponse(SipServletResponse message) throws ServletException, IOException {
    CallImpl call = Utils.getCall(message.getSession());
    call.doReponse(message);
  }

  @Override
  protected void doUpdate(SipServletRequest message) throws ServletException, IOException {
    CallImpl call = Utils.getCall(message.getSession());
    call.doUpdate(message);
  }
}
