package com.tropo.test.driver.impl.media;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.media.mscontrol.EventType;
import javax.media.mscontrol.MediaEventListener;
import javax.media.mscontrol.MediaSession;
import javax.media.mscontrol.MsControlException;
import javax.media.mscontrol.Parameter;
import javax.media.mscontrol.Parameters;
import javax.media.mscontrol.UnsupportedException;
import javax.media.mscontrol.mediagroup.MediaGroup;
import javax.media.mscontrol.mediagroup.Player;
import javax.media.mscontrol.mediagroup.PlayerEvent;
import javax.media.mscontrol.mediagroup.signals.SignalDetector;
import javax.media.mscontrol.mediagroup.signals.SignalDetectorEvent;
import javax.media.mscontrol.mediagroup.signals.SpeechRecognitionEvent;
import javax.media.mscontrol.resource.RTC;

import org.apache.log4j.Logger;

import com.tropo.test.driver.impl.CallImpl;

public class MediaService {
  private static final Logger LOG = Logger.getLogger(MediaService.class);

  protected CallImpl _parent;

  protected MediaSession _session;

  protected MediaGroup _group;

  protected Player _player;

  protected SignalDetector _detector;

  protected Lock playLock = new ReentrantLock();

  protected Condition playCompleteCondition = playLock.newCondition();

  protected PlayerEvent playerEvent;

  protected Lock detectorLock = new ReentrantLock();

  protected Condition detectorCompleteCondition = detectorLock.newCondition();

  protected SignalDetectorEvent detectorEvent;

  protected boolean released;

  public MediaService(final CallImpl parent, final MediaGroup group) {
    _parent = parent;
    _group = group;
    _session = group.getMediaSession();
  }

  protected synchronized Player getPlayer() {
    if (_player == null) {
      try {
        _player = _group.getPlayer();
        _player.addListener(new PlayerListener());
      }
      catch (UnsupportedException ex) {
        LOG.error("", ex);
        throw new UnsupportedOperationException("player is not supported by " + _group);
      }
      catch (MsControlException e) {
        LOG.error("", e);
        throw new RuntimeException(e);
      }

      if (_player == null) {
        throw new UnsupportedOperationException("Can't get Player.");
      }
    }

    return _player;
  }

  protected synchronized SignalDetector getSignalDetector() {
    if (_detector == null) {
      try {
        _detector = _group.getSignalDetector();
        _detector.addListener(new SignalDetectorListener());
      }
      catch (UnsupportedException ex) {
        LOG.error("", ex);
        throw new UnsupportedOperationException("SignalDetector is not supported by " + _group);
      }
      catch (MsControlException e) {
        LOG.error("", e);
        throw new RuntimeException(e);
      }

      if (_detector == null) {
        throw new UnsupportedOperationException("Can't get SignalDetector.");
      }
    }

    return _detector;
  }

  public void say(String text) {
    playLock.lock();
    try {
      getPlayer().play(createTTSResource(text), RTC.NO_RTC, null);
      while (playerEvent == null && !released) {
        playCompleteCondition.await();
      }
    }
    catch (Exception e) {
      LOG.error("Exception when playing", e);
    }
    finally {
      playerEvent = null;
      playLock.unlock();
    }
  }

  public void sayDTMF(String text) {
    playLock.lock();
    try {
      getPlayer().play(createDTMFTTSResource(text), RTC.NO_RTC, null);
      while (playerEvent == null && !released) {
        playCompleteCondition.await();
      }
    }
    catch (Exception e) {
      LOG.error("Exception when playing", e);
    }
    finally {
      playerEvent = null;
      playLock.unlock();
    }
  }

  public String ask(String grammer) {
    return this.ask(grammer, null, 0);
  }

  public String ask(String grammer, long initialTimeout) {
    return this.ask(grammer, null, initialTimeout);
  }

  public String ask(String grammer, Runnable run) {
    return this.ask(grammer, run, 0);
  }

  public String ask(String grammer, Runnable run, long initialTimeout) {
    detectorLock.lock();
    try {
      final Parameters patterns = _group.createParameters();
      patterns.put(SignalDetector.PATTERN[0], grammer);
      if (initialTimeout > 0) {
        patterns.put(SignalDetector.INITIAL_TIMEOUT, initialTimeout);
      }
      getSignalDetector().receiveSignals(-1, new Parameter[] {SignalDetector.PATTERN[0]}, RTC.NO_RTC, patterns);
      if (run != null) {
        run.run();
      }
      while (detectorEvent == null && !released) {
        detectorCompleteCondition.await();
      }
      if (detectorEvent.isSuccessful() && detectorEvent.getQualifier() == SignalDetectorEvent.PATTERN_MATCHING[0]) {
        String result = null;
        if (detectorEvent instanceof SpeechRecognitionEvent) {
          result = ((SpeechRecognitionEvent) detectorEvent).getTag();
        }
        else {
          result = detectorEvent.getSignalString();
        }

        return result;
      }
    }
    catch (Exception ex) {
      LOG.error("Exception when detecting", ex);
    }
    finally {
      detectorEvent = null;
      detectorLock.unlock();
    }
    return null;
  }

  private java.net.URI createTTSResource(final String text) throws UnsupportedEncodingException {
    return java.net.URI.create("data:" + URLEncoder.encode(
        "application/ssml+xml," + "<?xml version=\"1.0\"?>" + "<speak>" + "<voice>" + text + "</voice>" + "</speak>",
        "UTF-8"));
  }

  public static java.net.URI createDTMFTTSResource(final String text) throws UnsupportedEncodingException {
    return java.net.URI.create("data:" + URLEncoder.encode("application/ssml+xml," + "<?xml version=\"1.0\"?>"
        + "<speak>" + "<audio src=\"dtmf:" + text + "\"></audio>" + "</speak>", "UTF-8"));
  }

  public void release() {
    released = true;
    playLock.lock();
    try {
      playCompleteCondition.signalAll();
    }
    finally {
      playLock.unlock();
    }

    detectorLock.lock();
    try {
      detectorCompleteCondition.signalAll();
    }
    finally {
      detectorLock.unlock();
    }
  }

  protected class PlayerListener implements MediaEventListener<PlayerEvent> {
    public void onEvent(final PlayerEvent e) {
      playLock.lock();
      try {
        final EventType type = e.getEventType();
        if (type == PlayerEvent.PLAY_COMPLETED) {
          playerEvent = e;
          playCompleteCondition.signalAll();
        }
      }
      finally {
        playLock.unlock();
      }
    }
  }

  class SignalDetectorListener implements MediaEventListener<SignalDetectorEvent> {
    public void onEvent(final SignalDetectorEvent event) {
      detectorLock.lock();
      try {
        final EventType type = event.getEventType();
        if (type == SignalDetectorEvent.RECEIVE_SIGNALS_COMPLETED) {
          detectorEvent = event;
          detectorCompleteCondition.signalAll();
        }
      }
      finally {
        detectorLock.unlock();
      }
    }
  }

}
