/**
 * Copyright 2010 Voxeo Corporation Licensed under the Apache License, Version
 * 2.0 (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law
 * or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */

package com.tropo.test.driver.impl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Map;
import java.util.regex.PatternSyntaxException;

import javax.media.mscontrol.MediaErr;
import javax.media.mscontrol.networkconnection.SdpPortManagerEvent;
import javax.servlet.sip.Rel100Exception;
import javax.servlet.sip.ServletParseException;
import javax.servlet.sip.SipApplicationSession;
import javax.servlet.sip.SipFactory;
import javax.servlet.sip.SipServletMessage;
import javax.servlet.sip.SipServletRequest;
import javax.servlet.sip.SipServletResponse;
import javax.servlet.sip.SipSession;
import javax.servlet.sip.SipURI;
import javax.servlet.sip.URI;

import org.apache.log4j.Logger;

public class SIPHelper {

  private static final Logger LOG = Logger.getLogger(SIPHelper.class);

  public static SipServletRequest createSipInitnalRequest(final SipFactory factory, final String method,
      final String from, final String to, final Map<String, String> headers, String requestUri, SipApplicationSession applicationSession) {
    SipServletRequest req = null;
    SipApplicationSession createdAppSession = null;
    try {
      if (applicationSession == null) {
        createdAppSession = factory.createApplicationSession();
      }
      req = factory
          .createRequest(applicationSession != null ? applicationSession : createdAppSession, method, from, to);
    }
    catch (Exception ex) {
      LOG.error("Exception when creating INVITE request.", ex);
      if (createdAppSession != null) {
        try {
          createdAppSession.invalidate();
        }
        catch (Exception e) {
          LOG.error("Exception when invalidating application session:" + createdAppSession);
        }
      }
      throw new RuntimeException(ex);
    }

    Map<String, String> clones = null;
    if (headers != null) {
      clones = new HashMap<String, String>();
      clones.putAll(headers);
      for (final Map.Entry<String, String> e : clones.entrySet()) {
        if (e.getKey().equalsIgnoreCase("Route")) {
          try {
            String[] values = e.getValue().split("\\|\\|\\|");
            int length = values.length;
            for (int i = length - 1; i >= 0; i--) {
              LOG.debug("route[" + i + "]: " + values[i]);
              try {
                req.pushRoute(factory.createAddress(values[i]));
              }
              catch (ServletParseException ex) {
                LOG.error("Invalid Route Header: " + values[i]);
              }
            }
          }
          catch (PatternSyntaxException ex) {
            LOG.error(ex);
          }
        }
      }
      clones.remove("Route");
      clones.remove("route");
    }
    
    if (requestUri != null) {
      try {
        req.setRequestURI(factory.createURI(requestUri));
      }
      catch (ServletParseException e) {
        LOG.error("Exception when setting request URI with value " + requestUri);
        if (createdAppSession != null) {
          try {
            createdAppSession.invalidate();
          }
          catch (Exception ex) {
            LOG.error("Exception when invalidating application session:" + createdAppSession);
          }
        }
        throw new RuntimeException(e);
      }
    }
    
    SIPHelper.addHeaders(req, clones);
    return req;
  }

  public static void addHeaders(final SipServletMessage message, final Map<String, String> headers) {
    if (headers != null) {
      for (final Map.Entry<String, String> e : headers.entrySet()) {
        message.addHeader(e.getKey(), e.getValue());
      }
    }
  }

  public static byte[] getRawContentWOException(final SipServletMessage msg) {
    try {
      return msg.getRawContent();
    }
    catch (final IOException e) {
      LOG.warn("", e);
      return null;
    }
  }

  public static void copyContent(final SipServletMessage source, final SipServletMessage target) {
    try {
      final byte[] content = source.getRawContent();
      if (content != null) {
        target.setContent(content, source.getContentType());
      }
    }
    catch (final Throwable t) {
      throw new IllegalArgumentException(t);
    }
  }

  public static void setContent(final SipServletMessage message, final Object content, String contentType) {
    if (contentType == null) {
      contentType = "application/sdp";
    }
    try {
      message.setContent(content, contentType);
    }
    catch (UnsupportedEncodingException e) {
      throw new IllegalArgumentException(e);
    }
  }

  public static boolean isProvisionalResponse(final SipServletResponse res) {
    return res.getStatus() < 200;
  }

  public static boolean isSuccessResponse(final SipServletResponse res) {
    return res.getStatus() >= 200 && res.getStatus() <= 299;
  }

  public static boolean isErrorResponse(final SipServletResponse res) {
    return res.getStatus() >= 300;
  }

  public static boolean isBusy(final SipServletResponse res) {
    return res.getStatus() == SipServletResponse.SC_BUSY_HERE
        || res.getStatus() == SipServletResponse.SC_BUSY_EVERYWHERE;
  }

  public static boolean isDecline(final SipServletResponse res) {
    return res.getStatus() == SipServletResponse.SC_DECLINE;
  }

  public static boolean isNotAcceptableHere(final SipServletResponse res) {
    return res.getStatus() == SipServletResponse.SC_NOT_ACCEPTABLE_HERE;
  }

  public static boolean isTimeout(final SipServletResponse res) {
    return res.getStatus() == SipServletResponse.SC_REQUEST_TIMEOUT;
  }

  public static boolean isRedirect(final SipServletResponse res) {
    return res.getStatus() >= 300 && res.getStatus() <= 399;
  }

  public static boolean isInvite(final SipServletMessage msg) {
    return msg.getMethod().equalsIgnoreCase("INVITE");
  }

  public static boolean isInitial(final SipServletRequest req) {
    return req.isInitial();
  }

  public static boolean isReinvite(final SipServletMessage msg) {
    if (msg instanceof SipServletRequest) {
      return msg.getMethod().equalsIgnoreCase("INVITE") && !((SipServletRequest) msg).isInitial();
    }
    else {
      return msg.getMethod().equalsIgnoreCase("INVITE") && !((SipServletResponse) msg).getRequest().isInitial();
    }
  }

  public static boolean isAck(final SipServletMessage msg) {
    return msg.getMethod().equalsIgnoreCase("ACK");
  }

  public static boolean isCancel(final SipServletMessage msg) {
    return msg.getMethod().equalsIgnoreCase("CANCEL");
  }

  public static boolean isBye(final SipServletMessage msg) {
    return msg.getMethod().equalsIgnoreCase("BYE");
  }

  public static boolean isPrack(final SipServletMessage msg) {
    return msg.getMethod().equalsIgnoreCase("PRACK");
  }

  public static boolean isRegister(final SipServletMessage msg) {
    return msg.getMethod().equalsIgnoreCase("REGISTER");
  }

  public static boolean isUpdate(final SipServletMessage msg) {
    return msg.getMethod().equalsIgnoreCase("UPDATE");
  }

  public static void handleErrorSdpPortManagerEvent(final SdpPortManagerEvent event, final SipServletRequest req) {
    final MediaErr error = event.getError();
    try {
      if (SdpPortManagerEvent.SDP_NOT_ACCEPTABLE.equals(error) || MediaErr.NOT_SUPPORTED.equals(error)) {
        // Send 488 error response to INVITE
        req.createResponse(SipServletResponse.SC_NOT_ACCEPTABLE_HERE).send();
      }
      else if (SdpPortManagerEvent.RESOURCE_UNAVAILABLE.equals(error)) {
        // Send 486 error response to INVITE
        req.createResponse(SipServletResponse.SC_BUSY_HERE).send();
      }
      else {
        // Some unknown error. Send 500 error response to INVITE
        req.createResponse(SipServletResponse.SC_SERVER_INTERNAL_ERROR).send();
      }
    }
    catch (final IOException e) {
      LOG.warn("IOException when sending error response ", e);
    }
  }

  public static void sendReinvite(final SipSession session, final SipServletMessage origReq,
      final Map<String, String> headers) throws IOException {
    final SipServletRequest reinvite = session.createRequest("INVITE");
    SIPHelper.addHeaders(reinvite, headers);
    if (origReq != null) {
      SIPHelper.copyContent(origReq, reinvite);
    }
    reinvite.send();
  }

  public static boolean isContainSDP(final SipServletMessage req) {
    try {
      if (req.getContent() == null) {
        return false;
      }
      else {
        return true;
      }
    }
    catch (final Throwable t) {
      return false;
    }
  }

  public static URI getCleanUri(URI uri) {
    if (uri.isSipURI()) {
      SipURI sipURI = (SipURI) uri.clone();
      Iterator<String> iterator = sipURI.getParameterNames();
      while (iterator != null && iterator.hasNext()) {
        iterator.next();
        iterator.remove();
      }
      return sipURI;
    }
    else {
      return uri;
    }
  }

  public static boolean support100rel(SipServletRequest req) {
    boolean result = false;
    ListIterator<String> values = req.getHeaders("Supported");

    while (values.hasNext()) {
      String value = values.next();
      if (value.equalsIgnoreCase("100rel")) {
        result = true;
        break;
      }
    }

    return result;
  }

  public static boolean needPrack(SipServletResponse resp) {
    if (resp.getStatus() > 199 || resp.getStatus() < 101) {
      return false;
    }

    boolean result = false;
    ListIterator<String> values = resp.getHeaders("Require");

    while (values.hasNext()) {
      String value = values.next();
      if (value.equalsIgnoreCase("100rel")) {
        result = true;
        break;
      }
    }

    return result;
  }

  public static void trySendPrack(SipServletResponse resp) throws IOException {
    if (!needPrack(resp)) {
      return;
    }

    try {
      resp.createPrack().send();
    }
    catch (Rel100Exception ex) {
      LOG.warn(ex.getMessage());
    }
    catch (IllegalStateException ex) {
      LOG.warn(ex.getMessage());
    }
  }

  public static void remove100relSupport(SipServletRequest req) {
    ListIterator<String> values = req.getHeaders("Supported");
    while (values.hasNext()) {
      String value = values.next();
      if (value.equalsIgnoreCase("100rel")) {
        values.remove();
      }
    }

    values = req.getHeaders("Require");
    while (values.hasNext()) {
      String value = values.next();
      if (value.equalsIgnoreCase("100rel")) {
        values.remove();
      }
    }
  }

  public static void copyPandXHeaders(SipServletMessage origReq, SipServletMessage req) {
    Iterator<String> headerNames = origReq.getHeaderNames();

    while (headerNames.hasNext()) {
      String headerName = headerNames.next();
      if (headerName.startsWith("P-") || headerName.startsWith("p-") || headerName.startsWith("X-")
          || headerName.startsWith("x-")) {
        req.removeHeader(headerName);
        ListIterator<String> values = origReq.getHeaders(headerName);
        while (values.hasNext()) {
          String headerValue = values.next();
          req.addHeader(headerName, headerValue);
        }
      }
    }
  }

  public static void copyHeader(String header, SipServletMessage origMessage, SipServletMessage targetMessage) {
    if (header == null || header.trim().isEmpty()) {
      return;
    }
    ListIterator<String> values = origMessage.getHeaders(header);
    if (values != null) {
      while (values.hasNext()) {
        targetMessage.addHeader(header, values.next());
      }
    }
  }
}
