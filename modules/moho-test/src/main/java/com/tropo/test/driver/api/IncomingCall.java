package com.tropo.test.driver.api;

import java.util.Map;

public interface IncomingCall extends Call {

  void sendProvisiningResponse(int code, boolean withSDP, boolean isReliable, Map<String, String> headers, byte[] content, String contentType, boolean createNewNc);
  
  void sendProvisiningResponse(int code, boolean withSDP, boolean isReliable, Map<String, String> headers, byte[] content, String contentType);

  void sendProvisiningResponse(int code, boolean withSDP, boolean isReliable, Map<String, String> headers);

  void acceptEarlyMedia(int code, Map<String, String> headers);

  void answer(Map<String, String> headers);

  void reject(int code, Map<String, String> headers);

}
