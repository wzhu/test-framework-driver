package com.tropo.test.driver.impl;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.servlet.sip.SipServletRequest;
import javax.servlet.sip.SipServletResponse;

import org.apache.log4j.Logger;

import com.tropo.test.driver.api.OutgoingCall;

public class OutgoingCallImpl extends CallImpl implements OutgoingCall {

  private static final Logger LOG = Logger.getLogger(OutgoingCallImpl.class);

  private boolean inviteTakeSDP;

  private Map<String, String> headers;

  private String requestUri;

  public OutgoingCallImpl(String from, String to, Map<String, String> headers, boolean inviteTakeSDP,
      CallManagerImpl callManager, String requestUri) {
    super(callManager);
    this.caller = from;
    this.callee = to;
    this.headers = headers;
    this.inviteTakeSDP = inviteTakeSDP;
    this.requestUri = requestUri;
  }

  @Override
  public synchronized void hangup(Map<String, String> headers) {
    if (getState() == State.INVITING || getState() == State.RINGING) {
      LOG.debug("Cancel outbound call " + this);
      try {
        SipServletRequest req = this.getOriginalRequest().createCancel();
        req.send();
        setState(State.CANCELED);
      }
      catch (Exception e) {
        LOG.error("Exception when sending CANCEL. " + this, e);
        setState(State.FAILED);
        throw new RuntimeException(e);
      }
    }
    else {
      super.hangup(headers);
    }
  }

  @Override
  public synchronized void waitEarlyMedia() {
    LOG.debug(this + " start waitEarlyMedia.");
    while (!isDisconnected() && !isAnswered() && !isInEarlyMedia()) {
      try {
        wait();
      }
      catch (InterruptedException e) {
        //
      }
    }
    LOG.debug(this + " finish waitEarlyMedia.");
  }

  @Override
  public synchronized void waitAnswer() {
    LOG.debug(this + " start waitAnswer.");
    while (!isDisconnected() && !isAnswered()) {
      try {
        wait();
      }
      catch (InterruptedException e) {
        //
      }
    }
    LOG.debug(this + " finish waitAnswer.");
  }

  public synchronized void startCall() {
    LOG.debug(this + " start call.");
    try {
      createRequest(caller, callee, headers);

      if (inviteTakeSDP) {
        byte[] sdp = processSDP(null);
        if (sdp == null) {
          LOG.error("Can't get SDP " + this);
          throw new RuntimeException("Can't get SDP, startCall Failed " + this);
        }
        SIPHelper.setContent(getOriginalRequest(), sdp, null);
      }
      getOriginalRequest().send();
      setState(State.INVITING);
    }
    catch (Exception e) {
      LOG.error("Exception when sending out INVITE. ", e);
      setState(State.FAILED);
      throw new RuntimeException(e);
    }
  }

  private SipServletRequest createRequest(String from, String to, Map<String, String> headers) {
    SipServletRequest request = null;
    try {
      request = SIPHelper.createSipInitnalRequest(getCallManager().getSipFactory(), "INVITE", from, to, headers,
          requestUri, null);

      setOriginalRequest(request);
      getSession().setHandler(getCallManager().getServlet().getServletName());
    }
    catch (final Exception e) {
      if (request != null && request.getSession() != null) {
        try {
          request.getSession().invalidate();
        }
        catch (Exception ex) {
          LOG.error("Exception when invalidating session:" + request);
        }
      }
      throw new RuntimeException(e);
    }

    return request;
  }

  @Override
  public SipServletResponse waitForProvisiningResponse() throws InterruptedException {
    return incomingCallQueue.take();
  }

  @Override
  public SipServletResponse waitForProvisiningResponse(int timeout) throws InterruptedException {
    return incomingCallQueue.poll(timeout, TimeUnit.MILLISECONDS);
  }
}
