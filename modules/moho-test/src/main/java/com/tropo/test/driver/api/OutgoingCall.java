package com.tropo.test.driver.api;

import javax.servlet.sip.SipServletResponse;

public interface OutgoingCall extends Call {

  void waitEarlyMedia();

  void waitAnswer();

  SipServletResponse waitForProvisiningResponse() throws InterruptedException;

  SipServletResponse waitForProvisiningResponse(int timeout) throws InterruptedException;
}
