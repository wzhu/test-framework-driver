package com.tropo.test.driver.api;

public interface IncomingCallListener {

  void onIncomingCall(IncomingCall call);

}
