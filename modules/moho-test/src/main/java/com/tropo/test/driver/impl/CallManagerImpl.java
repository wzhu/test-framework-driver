package com.tropo.test.driver.impl;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import javax.media.mscontrol.MsControlFactory;
import javax.sdp.SdpFactory;
import javax.servlet.sip.SipFactory;
import javax.servlet.sip.SipServlet;
import javax.servlet.sip.SipSession;

import org.apache.log4j.Logger;

import com.tropo.test.driver.api.Call;
import com.tropo.test.driver.api.CallManager;
import com.tropo.test.driver.api.IncomingCall;
import com.tropo.test.driver.api.IncomingCallListener;
import com.tropo.test.driver.api.OutgoingCall;

public class CallManagerImpl implements CallManager {

  private static final Logger LOG = Logger.getLogger(CallManagerImpl.class);

  private Map<String, Call> calls = new ConcurrentHashMap<String, Call>();

  private Map<SipSession, Call> sipSessionCalls = new ConcurrentHashMap<SipSession, Call>();

  private BlockingQueue<IncomingCall> incomingCallQueue = new LinkedBlockingQueue<IncomingCall>(10);

  protected SipFactory _sipFactory;

  protected SdpFactory _sdpFactory;

  protected MsControlFactory _msControlFactory;

  protected SipServlet _servlet;

  protected List<IncomingCallListener> incomingCallListeners = new LinkedList<IncomingCallListener>();

  public CallManagerImpl(SipFactory sipFactory, SdpFactory sdpFactory, MsControlFactory msControlFactory,
      SipServlet servlet) {
    super();
    this._sipFactory = sipFactory;
    this._sdpFactory = sdpFactory;
    this._msControlFactory = msControlFactory;
    this._servlet = servlet;
  }

  @Override
  public OutgoingCall createOutgoingcall(String from, String to, boolean inviteTakeSDP, Map<String, String> headers,
      String requestUri) {
    LOG.debug("Creating outgoing call...");
    OutgoingCallImpl outgoingCall = new OutgoingCallImpl(from, to, headers, inviteTakeSDP, this, requestUri);
    outgoingCall.startCall();
    return outgoingCall;
  }

  public void reset() {
    incomingCallQueue.clear();
    calls.clear();
  }

  @Override
  public Collection<Call> getCalls() {
    return calls.values();
  }

  @Override
  public IncomingCall waitForIncomingCall() throws InterruptedException {
    return incomingCallQueue.poll(30, TimeUnit.SECONDS);
  }

  public SipFactory getSipFactory() {
    return _sipFactory;
  }

  public SdpFactory getSdpFactory() {
    return _sdpFactory;
  }

  public MsControlFactory getMsControlFactory() {
    return _msControlFactory;
  }

  public SipServlet getServlet() {
    return _servlet;
  }

  public void removeCall(Call call) {
    LOG.debug("Removing call " + call);
    calls.remove(call.getID());
    sipSessionCalls.remove(((CallImpl) call).getSession());
  }

  public void addCall(Call call) {
    LOG.debug("Adding call " + call);
    calls.put(call.getID(), call);
    sipSessionCalls.put(((CallImpl) call).getSession(), call);
    if (call instanceof IncomingCall) {
      this.receivedIncomingCall((IncomingCall) call);
    }
  }

  public Call getCallBySipSession(SipSession session) {
    return sipSessionCalls.get(session);
  }

  private void receivedIncomingCall(IncomingCall incomingCall) {
    LOG.debug("Received incoming call " + incomingCall);
    for (IncomingCallListener listener : incomingCallListeners) {
      try {
        listener.onIncomingCall(incomingCall);
      }
      catch (Throwable t) {
        LOG.error("Exception when notifying incoming call for " + listener, t);
      }
    }

    try {
      incomingCallQueue.put(incomingCall);
    }
    catch (InterruptedException e) {
      //
    }
  }

  @Override
  public OutgoingCall createOutgoingcall(String from, String dest, boolean inviteTakeSDP, Map<String, String> headers) {
    return createOutgoingcall(from, dest, inviteTakeSDP, headers, null);
  }

  @Override
  public void addIncomingCallLisener(IncomingCallListener listener) {
    incomingCallListeners.add(listener);
  }

  @Override
  public void removeIncomingCallLisener(IncomingCallListener listener) {
    incomingCallListeners.remove(listener);
  }
}
