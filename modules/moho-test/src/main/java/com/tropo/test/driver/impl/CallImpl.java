package com.tropo.test.driver.impl;

import java.io.IOException;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.media.mscontrol.MediaEventListener;
import javax.media.mscontrol.MediaSession;
import javax.media.mscontrol.MsControlException;
import javax.media.mscontrol.MsControlFactory;
import javax.media.mscontrol.Parameters;
import javax.media.mscontrol.join.Joinable.Direction;
import javax.media.mscontrol.mediagroup.MediaGroup;
import javax.media.mscontrol.networkconnection.NetworkConnection;
import javax.media.mscontrol.networkconnection.SdpPortManagerEvent;
import javax.servlet.sip.SipApplicationSession;
import javax.servlet.sip.SipServletMessage;
import javax.servlet.sip.SipServletRequest;
import javax.servlet.sip.SipServletResponse;
import javax.servlet.sip.SipSession;
import javax.servlet.sip.SipURI;
import javax.servlet.sip.TelURL;
import javax.servlet.sip.URI;

import org.apache.log4j.Logger;

import com.tropo.test.driver.api.Call;
import com.tropo.test.driver.api.Configuration;
import com.tropo.test.driver.impl.media.MediaService;

public class CallImpl implements Call, MediaEventListener<SdpPortManagerEvent> {

  private static final Logger LOG = Logger.getLogger(CallImpl.class);

  private String id = UUID.randomUUID().toString();

  private byte[] _localSDP;

  private byte[] _remoteSDP;

  private SipServletRequest _originalRequest;

  private State _state = State.NEW;

  private CallManagerImpl _callManager;

  private byte[] sdpProcessResult;

  private SDPState sdpProcessState;

  private boolean inEarlyMedia;

  private MediaService mediaService;

  protected BlockingQueue<SipServletResponse> incomingCallQueue = new LinkedBlockingQueue<SipServletResponse>(10);

  protected SipServletResponse successResponse = null;

  protected SipServletRequest receivedReInvite;
  
  protected String caller;
  
  protected String callee;

  protected enum SDPState {
    GneratingSDPOffer, GeneratedSDPOffer, ProcessingSDPOffer, ProcessingSDPAnswer, ProcessedSDPOfer, ProcessedSDPAnswer
  }

  public CallImpl(CallManagerImpl _callManager) {
    super();
    this._callManager = _callManager;
  }

  @Override
  public String getID() {
    return id;
  }

  @Override
  public State getState() {
    return _state;
  }

  @Override
  public byte[] getLocalSDP() {
    return _localSDP;
  }

  @Override
  public byte[] getRemoteSDP() {
    return _remoteSDP;
  }

  public SipServletRequest getOriginalRequest() {
    return _originalRequest;
  }

  public void setOriginalRequest(SipServletRequest _originalRequest) {
    this._originalRequest = _originalRequest;
    Utils.setCall(_originalRequest.getSession(), this);
  }

  public SipSession getSession() {
    return getOriginalRequest() != null ? getOriginalRequest().getSession() : null;
  }

  public CallManagerImpl getCallManager() {
    return _callManager;
  }

  protected synchronized void setState(State state) {
    if (_state == state) {
      return;
    }
    if (isDisconnected()) {
      LOG.warn("Already terminated " + this);
      return;
    }
    State oldState = _state;
    _state = state;

    stateChanged(oldState, _state);
  }

  private void stateChanged(State oldState, State currentState) {
    LOG.debug(this + " state changed from " + oldState + " to " + currentState);
    if (currentState == State.INVITING) {
      _callManager.addCall(this);
    }
    else if (isDisconnected()) {
      _callManager.removeCall(this);
      try {
        clearSipResource();
      }
      catch (Exception ex) {
        LOG.error("", ex);
      }

      try {
        clearMediaResource();
      }
      catch (Exception ex) {
        LOG.error("", ex);
      }
    }
    else if (currentState != State.RINGING) {
      inEarlyMedia = false;
    }
    notifyAll();
    // TODO need listener?
  }

  // /////////// media operations///////////////
  @Override
  public void say(String text) {
    getMediaService().say(text);
  }

  @Override
  public void sayDTMF(String dtmf) {
    getMediaService().sayDTMF(dtmf);
  }

  @Override
  public String ask(String grammar) {
    return getMediaService().ask(grammar);
  }

  public String ask(String grammar, long timeout) {
    return getMediaService().ask(grammar, timeout);
  }

  @Override
  public String ask(String grammar, long timeout, Runnable run) {
    return getMediaService().ask(grammar, run);
  }

  // /////////// signal operations///////////////
  protected SipServletResponse updateResp;

  @Override
  public synchronized void sendUpdate(Map<String, String> headers) {
    waitForProcessMessageComplete();
    operationInprogress = true;
    try {
      if (isInEarlyMedia() || getState() == State.ANSWERED) {
        try {
          SipServletRequest update = _originalRequest.getSession().createRequest("UPDATE");
          SIPHelper.addHeaders(update, headers);
          byte[] sdp = this.processSDP(null);
          SIPHelper.setContent(update, sdp, null);
          update.send();

          while (!isDisconnected() && updateResp == null) {
            try {
              wait();
            }
            catch (InterruptedException e) {
            }
          }

          updateResp = null;
        }
        catch (IOException e) {
          LOG.error("Exception when sending UPDATE", e);
          throw new RuntimeException(e);
        }
      }
      else {
        LOG.warn("Wrong state when sending UPDATE " + this);
      }
    }
    finally {
      operationInprogress = false;
    }
  }

  protected boolean operationInprogress;

  protected boolean processingMessages;

  protected synchronized void waitForProcessMessageComplete() {
    while (processingMessages && !isDisconnected()) {
      try {
        wait();
      }
      catch (InterruptedException e) {

      }
    }
  }

  protected synchronized void waitOperationComplete() {
    while (operationInprogress && !isDisconnected()) {
      try {
        wait();
      }
      catch (InterruptedException e) {

      }
    }
  }

  protected SipServletResponse reInviteResp;

  @Override
  public synchronized void reInvite(Map<String, String> headers) {
    waitForProcessMessageComplete();
    operationInprogress = true;
    try {
      if (getState() == State.ANSWERED) {
        try {
          SipServletRequest reInvite = _originalRequest.getSession().createRequest("INVITE");
          SIPHelper.addHeaders(reInvite, headers);
          this.processSDP(null);
          byte[] sdp = this.waitForProcessSDP();
          SIPHelper.setContent(reInvite, sdp, null);
          reInvite.send();

          while (!isDisconnected() && reInviteResp == null) {
            try {
              wait();
            }
            catch (InterruptedException e) {
            }
          }

          reInviteResp = null;
        }
        catch (IOException e) {
          LOG.error("Exception when sending re-INVITE", e);
          throw new RuntimeException(e);
        }
      }
      else {
        LOG.warn("Wrong state when sending re-INVITE " + this);
      }
    }
    finally {
      operationInprogress = false;
    }
  }

  // DOTO hangup / disconnect
  @Override
  public synchronized void hangup(Map<String, String> headers) {
    if (getState() == State.ANSWERED) {
      LOG.debug("Hangup " + this);
      try {
        SipServletRequest bye = getSession().createRequest("BYE");
        SIPHelper.addHeaders(bye, headers);
        bye.send();
        setState(State.DISCONNECTED);
      }
      catch (Exception e) {
        LOG.error("Exception when hangup", e);
        throw new RuntimeException(e);
      }
    }
    else {
      LOG.warn("Wrong state when hangup " + this);
    }
  }

  // ///////////process messages//////////////
  public synchronized void doUpdate(SipServletRequest request) {
    if (operationInprogress) {
      try {
        request.createResponse(SipServletResponse.SC_REQUEST_PENDING).send();
      }
      catch (IOException e) {
        LOG.error("Exception when processing UPDATE " + this, e);
      }
      return;
    }
    processingMessages = true;
    try {
      if (SIPHelper.isContainSDP(request)) {
        if (isSDPNegotiated()) {
          byte[] sdp = processSDP(request);
          SipServletResponse resp = request.createResponse(200);
          SIPHelper.setContent(resp, sdp, null);
          resp.send();
        }
        else {
          LOG.error("Wrong sdpProcessState " + this);
          try {
            request.createResponse(500).send();
          }
          catch (Exception ex) {
            LOG.error("Exception when sending response " + this, ex);
          }
          hangup(null);
        }
      }
      else {
        request.createResponse(200).send();
      }
    }
    catch (IOException ex) {
      LOG.error("Exception when processing UPDATE " + this, ex);
      hangup(null);
    }
    finally {
      processingMessages = false;
    }
  }

  public synchronized void doAck(SipServletRequest request) {
    try {
      if (SIPHelper.isContainSDP(request)) {
        processSDP(request);
      }

      if (this.getState() == State.RINGING || this.getState() == State.INVITING) {
        this.setState(State.ANSWERED);
      }
      else {
        ack = request;
        notifyAll();
      }
    }
    catch (Exception e) {
      try {
        request.createResponse(500).send();
      }
      catch (IOException e1) {
        LOG.error("Exception when processing ACK " + this, e1);
      }
      LOG.error("Exception when processing ACK " + this, e);
      hangup(null);
    }
  }

  protected SipServletRequest prack;

  protected synchronized void waitForPrack() {
    while (!isDisconnected() && prack == null) {
      try {
        wait();
      }
      catch (InterruptedException e) {

      }
    }
    prack = null;
  }

  public synchronized void doPrack(SipServletRequest request) {
    SipServletResponse resp = request.createResponse(200);
    if (SIPHelper.getRawContentWOException(request) != null) {
      byte[] sdp = processSDP(request);
      if (sdpProcessState == SDPState.ProcessedSDPOfer) {
        SIPHelper.setContent(resp, sdp, null);
      }
    }
    prack = request;
    try {
      resp.send();
    }
    catch (IOException e) {
      LOG.error("", e);
      hangup(null);
    }

    this.notifyAll();
  }

  protected SipServletRequest ack;

  public synchronized void doReInvite(SipServletRequest request) {
    receivedReInvite = request;
    if (operationInprogress) {
      try {
        request.createResponse(SipServletResponse.SC_REQUEST_PENDING).send();
      }
      catch (IOException e) {
        LOG.error("Exception when processing reINVITE " + this, e);
      }
      return;
    }
    processingMessages = true;
    try {
      if (isSDPNegotiated()) {
        byte[] sdp = processSDP(request);
        SipServletResponse resp = request.createResponse(200);
        SIPHelper.setContent(resp, sdp, null);
        resp.send();

        while (!isDisconnected() && ack == null) {
          try {
            wait();
          }
          catch (InterruptedException e) {

          }
        }
        ack = null;
      }
      else {
        LOG.error("Wrong sdpProcessState " + this);
        try {
          request.createResponse(500).send();
        }
        catch (Exception ex) {
          LOG.error("Exception when sending response " + this, ex);
        }
        hangup(null);
      }
    }
    catch (IOException ex) {
      LOG.error("Exception when processing UPDATE " + this, ex);
      hangup(null);
    }
    finally {
      processingMessages = false;
    }
  }

  public synchronized void doReponse(SipServletResponse response) {
    String method = response.getRequest().getMethod();
    if (method.equalsIgnoreCase("INVITE")) {
      if (SIPHelper.isProvisionalResponse(response)) {
        if (response.getStatus() == 100) {
          return;
        }

        if (getState() == State.INVITING) {
          setState(State.RINGING);
        }

        if (SIPHelper.needPrack(response)) {
          try {
            SipServletRequest prack = response.createPrack();
            if (SIPHelper.isContainSDP(response)) {
              byte[] sdp = processSDP(response);
              if (sdpProcessState == SDPState.ProcessedSDPOfer) {
                SIPHelper.setContent(prack, sdp, null);
              }
            }
            prack.send();
          }
          catch (Exception ex) {
            LOG.error("Exception when processing response " + this, ex);
            hangup(null);
            throw new RuntimeException();
          }
        }
        else if (SIPHelper.isContainSDP(getOriginalRequest()) && SIPHelper.isContainSDP(response)
            && Configuration.isEarlyMediaWithout100rel()) {
          processSDP(response);
        }

        try {
          incomingCallQueue.put(response);
        }
        catch (InterruptedException e) {
          LOG.warn("", e);
        }
      }
      else if (SIPHelper.isSuccessResponse(response)) {
        successResponse = response;
        SipServletRequest ack = response.createAck();
        if (inEarlyMedia || (getState() == State.RINGING && sdpProcessState == SDPState.ProcessedSDPAnswer)) {
          try {
            this.setState(State.ANSWERED);
            ack.send();
          }
          catch (IOException e) {
            LOG.error("Exception when processing response " + this, e);
            hangup(null);
            throw new RuntimeException();
          }
        }
        else if (SIPHelper.isContainSDP(response)) {
          try {
            byte[] sdp = processSDP(response);
            if (sdpProcessState == SDPState.ProcessedSDPOfer) {
              SIPHelper.setContent(ack, sdp, null);
            }
            this.setState(State.ANSWERED);
            ack.send();
          }
          catch (Exception e) {
            LOG.error("Exception when processing response " + this, e);
            hangup(null);
            throw new RuntimeException();
          }
        }
        else {
          LOG.error(this + " received response in wrong state " + response);
        }
      }
      else if (SIPHelper.isErrorResponse(response)) {
        setState(State.REJECTED);
      }
    }
    else if (method.equalsIgnoreCase("PRACK")) {
      if (SIPHelper.isSuccessResponse(response)) {
        successResponse = response;
        if (SIPHelper.isContainSDP(response)) {
          try {
            processSDP(response);
          }
          catch (Exception e) {
            LOG.error("Exception when processing response " + this, e);
            hangup(null);
            throw new RuntimeException();
          }
        }

        if (isSDPNegotiated() && getState() == State.RINGING) {
          inEarlyMedia = true;
          notifyAll();
        }
      }
      else if (SIPHelper.isErrorResponse(response)) {
        hangup(null);
      }
    }
    else if (method.equalsIgnoreCase("UPDATE")) {
      if (SIPHelper.isSuccessResponse(response)) {
        successResponse = response;
        if (SIPHelper.isContainSDP(response)) {
          try {
            processSDP(response);
          }
          catch (Exception e) {
            LOG.error("Exception when processing response " + this, e);
            hangup(null);
            throw new RuntimeException();
          }
        }
      }
    }
    else if (method.equalsIgnoreCase("BYE")) {

    }
    else if (method.equalsIgnoreCase("CANCEL")) {

    }
    else {
      LOG.warn(this + "Received unknown response " + response);
    }
  }

  public synchronized void doBye(SipServletRequest request) {
    try {
      request.createResponse(200).send();
    }
    catch (IOException e) {
      LOG.error(this + " Exception when sending response");
    }
    if (getState() == State.ANSWERED) {
      setState(State.DISCONNECTED);
    }
    else {
      setState(State.FAILED);
    }
  }

  public synchronized void doCancel(SipServletRequest request) {
    LOG.error(this + " processing CANCEL request.");
    if (getState() == State.ANSWERED) {
      setState(State.DISCONNECTED);
    }
    else {
      setState(State.CANCELED);
    }
  }

  protected MediaSession _media;

  protected NetworkConnection _network;

  protected synchronized byte[] processSDP(final SipServletMessage msg) {
    return processSDP(msg, false);
  }

  protected synchronized byte[] processSDP(final SipServletMessage msg, boolean createNewNC) {
    try {
      if (_network == null) {
        if (sdpProcessState == null) {
          createNetworkConnection();
        }
        else {
          throw new IllegalStateException("NetworkConnection is NULL " + this);
        }
      }
      else if (createNewNC) {
        clearMediaResource();
        createNetworkConnection();
      }
      final byte[] sdp = msg == null ? null : msg.getRawContent();
      if (sdp != null) {
        _remoteSDP = sdp;
      }
      if (sdpProcessState == null || isSDPNegotiated()) {
        if (sdp == null) {
          _network.getSdpPortManager().generateSdpOffer();
          sdpProcessState = SDPState.GneratingSDPOffer;
        }
        else {
          _network.getSdpPortManager().processSdpOffer(sdp);
          sdpProcessState = SDPState.ProcessingSDPOffer;
        }
      }
      else if (sdpProcessState == SDPState.GeneratedSDPOffer && sdp != null) {
        _network.getSdpPortManager().processSdpAnswer(sdp);
        sdpProcessState = SDPState.ProcessingSDPAnswer;
      }

      return waitForProcessSDP();
    }
    catch (final Exception e) {
      LOG.error(e);
      hangup(null);
      throw new RuntimeException(e);
    }
  }

  private synchronized byte[] waitForProcessSDP() {
    byte[] sdp = null;
    while (!isDisconnected() && sdp == null) {
      try {
        wait();
        sdp = sdpProcessResult;
      }
      catch (InterruptedException e) {
        //
      }
    }
    sdpProcessResult = null;
    return sdp;
  }

  @Override
  public synchronized void onEvent(SdpPortManagerEvent event) {
    LOG.debug("Received event " + event);
    if (event.isSuccessful()) {
      if (sdpProcessState == SDPState.GneratingSDPOffer) {
        sdpProcessState = SDPState.GeneratedSDPOffer;
      }
      else if (sdpProcessState == SDPState.ProcessingSDPAnswer) {
        sdpProcessState = SDPState.ProcessedSDPAnswer;
      }
      else if (sdpProcessState == SDPState.ProcessingSDPOffer) {
        sdpProcessState = SDPState.ProcessedSDPOfer;
      }

      sdpProcessResult = event.getMediaServerSdp();
      _localSDP = sdpProcessResult;
      notifyAll();
    }
    else {
      LOG.error(this + " received fail 309 event " + event);
      hangup(null);
    }
  }

  private synchronized void createNetworkConnection() throws MsControlException {
    if (_media == null) {
      final MsControlFactory mf = getCallManager().getMsControlFactory();

      _media = mf.createMediaSession();
    }
    if (_network == null) {
      Parameters params = _media.createParameters();

      _network = _media.createNetworkConnection(NetworkConnection.BASIC, params);
      _network.getSdpPortManager().addListener(this);
    }
  }

  protected MediaService getMediaService() {
    if (mediaService == null) {
      MediaGroup mediaGroup;
      try {
        mediaGroup = _media.createMediaGroup(MediaGroup.PLAYER_RECORDER_SIGNALDETECTOR, _media.createParameters());
        _network.join(Direction.DUPLEX, mediaGroup);
        mediaService = new MediaService(this, mediaGroup);
      }
      catch (MsControlException e) {
        LOG.error("Exception when creating media service " + this, e);
      }
    }

    return mediaService;
  }

  private void clearMediaResource() {
    if (LOG.isDebugEnabled()) {
      LOG.debug(this + " destroying NetworkConnection");
    }
    sdpProcessState = null;
    if (_network != null) {
      try {
        _network.release();
      }
      catch (final Throwable t) {
        LOG.warn(this + " exception when releasing networkconnection.", t);
      }
      _network = null;
    }

    if (_media != null) {
      try {
        _media.release();
      }
      catch (final Throwable t) {
        LOG.warn(this + " exception when releasing media object", t);
      }
      _media = null;
    }

    if (mediaService != null) {
      mediaService.release();
    }
  }

  private void clearSipResource() {
    if (_originalRequest != null) {
      SipApplicationSession appSession = _originalRequest.getApplicationSession();

      try {
        if (appSession.isReadyToInvalidate()) {
          appSession.invalidate();
          if (LOG.isDebugEnabled()) {
            LOG.debug(appSession.getId() + " invalidated");
          }
        }
      }
      catch (IllegalStateException doofus) {
        try {
          appSession.invalidate();
          if (LOG.isDebugEnabled()) {
            LOG.debug(appSession.getId() + " invalidated anyway");
          }
        }
        catch (Exception ex) {
          LOG.warn(this + " Exception caught while invalidating SipApplicationSession " + appSession.getId(), ex);
        }
      }
    }
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + "[" + this.getID() + "] " + this.getState() + " SipSession[" + this.getSession()
        + "] MediaSession[" + this._media + "]" + " sdpProcessState " + this.sdpProcessState;
  }

  public synchronized boolean isDisconnected() {
    return (getState() == State.DISCONNECTED || getState() == State.CANCELED || getState() == State.FAILED
        || getState() == State.REJECTED);
  }

  public synchronized boolean isAnswered() {
    return getState() == State.ANSWERED;
  }

  public synchronized boolean isInEarlyMedia() {
    return inEarlyMedia;
  }

  public synchronized SDPState getSdpProcessState() {
    return sdpProcessState;
  }

  public synchronized boolean isSDPNegotiated() {
    return (sdpProcessState == SDPState.ProcessedSDPOfer || sdpProcessState == SDPState.ProcessedSDPAnswer);
  }

  @Override
  public synchronized void waitForHangup() {
    while (!isDisconnected()) {
      try {
        this.wait();
      }
      catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  public SipServletRequest getReceivedReInvite() {
    return receivedReInvite;
  }

  public SipServletResponse getReceivedSuccessResponse() {
    return this.successResponse;
  }

  public String getCaller() {
    return caller;
  }

  public String getCallee() {
    return callee;
  }
  
  protected String extractUser(URI uri) {
    if(uri instanceof SipURI) {
      return ((SipURI)uri).getUser();
    }
    else {
      return ((TelURL)uri).getPhoneNumber();
    }
  }
}
