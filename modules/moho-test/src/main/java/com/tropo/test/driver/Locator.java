package com.tropo.test.driver;

import com.tropo.test.driver.api.CallManager;
import com.tropo.test.driver.impl.TestDriverServlet;

public class Locator {

  public static CallManager _callManager;

  public static TestDriverServlet _driverServlet;

  public static CallManager getCallManager() {
    return _callManager;
  }

  public static void setCallManager(CallManager _callManager) {
    Locator._callManager = _callManager;
  }

  public static TestDriverServlet getDriverServlet() {
    return _driverServlet;
  }

  public static void setDriverServlet(TestDriverServlet _driverServlet) {
    Locator._driverServlet = _driverServlet;
  }

}
