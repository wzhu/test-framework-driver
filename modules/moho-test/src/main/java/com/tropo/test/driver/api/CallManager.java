package com.tropo.test.driver.api;

import java.util.Collection;
import java.util.Map;

public interface CallManager {

  OutgoingCall createOutgoingcall(String from, String dest, boolean inviteTakeSDP, Map<String, String> headers);

  OutgoingCall createOutgoingcall(String from, String dest, boolean inviteTakeSDP, Map<String, String> headers,
      String requestUri);

  Collection<Call> getCalls();

  IncomingCall waitForIncomingCall() throws InterruptedException;

  void addIncomingCallLisener(IncomingCallListener listener);

  void removeIncomingCallLisener(IncomingCallListener listener);
}
