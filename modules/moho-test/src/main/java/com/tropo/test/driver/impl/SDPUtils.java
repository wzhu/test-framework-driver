package com.tropo.test.driver.impl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.sdp.MediaDescription;
import javax.sdp.SdpException;
import javax.sdp.SdpFactory;
import javax.sdp.SessionDescription;

import org.apache.log4j.Logger;

public class SDPUtils {
  private static final Logger LOG = Logger.getLogger(SDPUtils.class);

  private static SdpFactory _sdpFactory;

  public static void init(SdpFactory sdpFactory) {
    _sdpFactory = sdpFactory;
  }

  public static byte[] makeBlackholeSDP(byte[] sdp) throws IOException, SdpException {
    SessionDescription sd = _sdpFactory.createSessionDescription(new String((byte[]) sdp, "iso8859-1"));
    if (sd.getConnection() != null) {
      sd.getConnection().setAddress("0.0.0.0");
    }

    MediaDescription md = (MediaDescription) sd.getMediaDescriptions(false).get(0);
    if (md.getConnection() != null) {
      md.getConnection().setAddress("0.0.0.0");
    }

    return sd.toString().getBytes("iso8859-1");
  }

  public SessionDescription createSendonlySDP(final byte[] sdpByte) throws UnsupportedEncodingException, SdpException {
    SessionDescription sd = _sdpFactory.createSessionDescription(new String(sdpByte, "iso8859-1"));

    sd.removeAttribute("sendrecv");
    sd.removeAttribute("recvonly");

    MediaDescription md = ((MediaDescription) sd.getMediaDescriptions(false).get(0));
    md.removeAttribute("sendrecv");
    md.removeAttribute("recvonly");
    md.setAttribute("sendonly", null);

    return sd;
  }
}
