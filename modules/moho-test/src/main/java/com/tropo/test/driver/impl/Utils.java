package com.tropo.test.driver.impl;

import javax.servlet.sip.SipSession;

public class Utils {

  public static CallImpl getCall(SipSession session) {
    return (CallImpl) session.getAttribute("com.voxeo.moho.functionaltest.call");
  }

  public static void setCall(SipSession session, CallImpl call) {
    session.setAttribute("com.voxeo.moho.functionaltest.call", call);
  }

}
