package com.tropo.test.driver;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.concurrent.Future;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.tropo.test.driver.impl.RequestImpl;

public class DriverServlet extends HttpServlet {
  private static final Logger LOG = Logger.getLogger(DriverServlet.class);

  private static final long serialVersionUID = 6799426773255342391L;

  @Override
  public void init() throws ServletException {
    super.init();
    ServiceLoader<Driver> loader = ServiceLoader.load(Driver.class);
    loader.iterator();
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String driverName = req.getParameter(Request.DRIVER_NAME);
    String instanceName = req.getParameter(Request.INSTANCE_NAME);

    if (driverName == null || instanceName == null) {
      resp.sendError(400, "Have no driverName or instanceName parameter.");
    }

    Map<String, String> parameters = new HashMap<String, String>();
    Enumeration names = req.getParameterNames();
    while (names.hasMoreElements()) {
      String name = (String) names.nextElement();
      parameters.put(name, req.getParameter(name));
    }

    Request request = new RequestImpl(driverName, instanceName, parameters);

    LOG.debug("Received request:" + request);

    ServiceLoader<Driver> loader = ServiceLoader.load(Driver.class, Request.class.getClassLoader());
    Iterator<Driver> drivers = loader.iterator();

    Future<Result> resultFuture = null;
    while (drivers.hasNext()) {
      Driver driver = drivers.next();
      Instance instance = driver.getInstance(request);
      if (instance != null) {
        resultFuture = instance.start();
        break;
      }
    }

    Result result = null;
    try {
      result = resultFuture.get();
    }
    catch (Exception e) {
      LOG.error("Exception when getting result.", e);
    }

    if (result != null) {
      resp.setStatus(result.getStatus());
      if (result.getDescription() != null) {
        resp.getOutputStream().println(result.getDescription());
      }
      if (result.getError() != null) {
        resp.getOutputStream().println(result.getError().toString());
      }
    }
    else {
      resp.setStatus(500);
    }
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    doGet(req, resp);
  }

  @Override
  public void destroy() {
    super.destroy();
  }
}
