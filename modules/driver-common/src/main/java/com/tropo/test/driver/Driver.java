package com.tropo.test.driver;

public interface Driver {

  Instance getInstance(Request request);

  String getName();
}
