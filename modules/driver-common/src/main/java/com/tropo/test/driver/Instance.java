package com.tropo.test.driver;

import java.util.concurrent.Future;

public interface Instance {

  Future<Result> start();

}
