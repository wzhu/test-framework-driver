package com.tropo.test.driver;

import java.util.Map;

public interface Request {

  public static final String DRIVER_NAME = "drivername";

  public static final String INSTANCE_NAME = "instancename";

  public static final String SCRIPT_BASE_URL = "scriptbaseurl";

  public static final String DRIVER_SCRIPT = "driverscript";

  String getDriverName();

  String getApplicationName();

  Map<String, String> getParameters();
}
