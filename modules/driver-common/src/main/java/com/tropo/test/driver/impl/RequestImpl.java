package com.tropo.test.driver.impl;

import java.util.Map;

import com.tropo.test.driver.Request;

public class RequestImpl implements Request {

  String driverName;

  String applicationName;

  Map<String, String> parameters;

  public RequestImpl(String driverName, String applicationName, Map<String, String> parameters) {
    super();
    this.driverName = driverName;
    this.applicationName = applicationName;
    this.parameters = parameters;
  }

  public String getDriverName() {
    return driverName;
  }

  public Map<String, String> getParameters() {
    return parameters;
  }

  public String getApplicationName() {
    return applicationName;
  }

  @Override
  public String toString() {
    return "RequestImpl [driverName=" + driverName + ", applicationName=" + applicationName + ", parameters="
        + parameters + "]";
  }
}
