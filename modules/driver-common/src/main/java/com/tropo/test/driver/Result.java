package com.tropo.test.driver;

public interface Result {
  /**
   * 0 success 1 fail
   * 
   * @return
   */
  int getStatus();

  Throwable getError();

  String getDescription();
}
