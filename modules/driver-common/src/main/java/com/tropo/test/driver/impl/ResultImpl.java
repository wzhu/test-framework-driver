package com.tropo.test.driver.impl;

import com.tropo.test.driver.Result;

public class ResultImpl implements Result {

  private int status;

  private Throwable error;

  private String description;

  public ResultImpl(int status, Throwable error, String description) {
    super();
    this.status = status;
    this.error = error;
    this.description = description;
  }

  public int getStatus() {
    return status;
  }

  public Throwable getError() {
    return error;
  }

  public String getDescription() {
    return description;
  }
}
