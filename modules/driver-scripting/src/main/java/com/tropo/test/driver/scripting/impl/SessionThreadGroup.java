package com.tropo.test.driver.scripting.impl;

import java.lang.ref.WeakReference;

import org.apache.log4j.Logger;

public class SessionThreadGroup extends ThreadGroup {
  private static final Logger LOG = Logger.getLogger(SessionThreadGroup.class);

  private WeakReference<InstanceImpl> appInstance;

  SessionThreadGroup(ThreadGroup parent, InstanceImpl appInstance, String sessionId) {
    super(parent, "Tropo-ThreadGroup-" + sessionId);
    setDaemon(true);
    this.appInstance = new WeakReference<InstanceImpl>(appInstance);
  }

  public InstanceImpl getApplicationInstance() {
    return appInstance.get();
  }

  public boolean allowThreadSuspension(boolean flag) {
    return false;
  }

  public void uncaughtException(Thread t, Throwable e) {
    while (e.getCause() != null) {
      LOG.error(e.getClass().getName() + " (" + e.getMessage() + ") wraps another exception");
      e = e.getCause();
    }
    if (!(e instanceof ThreadDeath)) {
      LOG.warn("SessionThreadGroup notified of an uncaught Throwable on thread " + t.getName(), e);
    }
  }

  public void threadCreated(Thread thread) {
    if (this != thread.getThreadGroup()) {
      throw new IllegalStateException("Thread " + thread.getName() + " is not a member of thread group " + getName());
    }
    InstanceImpl session = appInstance.get();
    if (session != null) {
      session.threadSpawned(thread);
    }
  }
}
