package com.tropo.test.driver.scripting.impl;

import javax.script.Bindings;
import javax.script.CompiledScript;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptException;

public class SimulatedCompiledScript extends CompiledScript {
  InstanceManager _mgr;

  Application _app;

  String _source;

  String _shimSource;

  String _type;

  ScriptEngine eng;

  public SimulatedCompiledScript(InstanceManager mgr, String source, Application app) {
    super();
    _mgr = mgr;
    _app = app;
    _type = app.getScriptType();
    _source = "# encoding: UTF-8".concat("\r\n").concat(source);
  }

  public SimulatedCompiledScript(InstanceManager mgr, String source, String shimSource, Application app) {
    this(mgr, source, app);
    _shimSource = shimSource;
  }

  @Override
  public Object eval() throws ScriptException {
    eng = getEngine();
    if (eng == null) {
      throw new RuntimeException(
          "Can not execute " + this + " because engine pool is exhausted or the application type is not supported.");
    }

    if (_shimSource != null) {
      eng.eval(_shimSource);
    }
    setTerminateOnExit();
    return eng.eval(_source);

  }

  @Override
  public Object eval(Bindings bindings) throws ScriptException {
    // wait at most 10s to try to get a valid script engine
    eng = getEngine();
    if (eng == null) {
      throw new RuntimeException(
          "Can not execute " + this + " because engine pool is exhausted or the application type is not supported.");
    }

    if (_shimSource != null) {
      eng.eval(_shimSource, bindings);
      setTerminateOnExit();
      return eng.eval(_source);
    }
    else {
      setTerminateOnExit();
      return eng.eval(_source, bindings);
    }
  }

  @Override
  public Object eval(ScriptContext context) throws ScriptException {
    // wait at most 10s to try to get a valid script engine
    eng = getEngine();
    if (eng == null) {
      throw new RuntimeException(
          "Can not execute " + this + " because engine pool is exhausted or the application type is not supported.");
    }

    if (_shimSource != null) {
      eng.eval(_shimSource, context);
      setTerminateOnExit();
      return eng.eval(_source);
    }
    else {
      setTerminateOnExit();
      return eng.eval(_source, context);
    }
  }

  @Override
  public ScriptEngine getEngine() {
    if (eng == null) {
      eng = _mgr.getScriptEngine(_app);
    }
    return eng;
  }

  public void setTerminateOnExit() {
    // Set JRubyEngine to terminates the state as well as executes at_exit
    // blocks for every script evaluation
    if (_type.equalsIgnoreCase("jruby")) {
      getEngine().getContext().setAttribute("org.jruby.embed.termination", true, ScriptContext.ENGINE_SCOPE);
    }
  }

}
