package com.tropo.test.driver.scripting.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import org.apache.commons.pool.ObjectPool;
import org.apache.commons.pool.PoolUtils;
import org.apache.commons.pool.PoolableObjectFactory;
import org.apache.commons.pool.impl.StackObjectPool;
import org.apache.log4j.Logger;

public class ScriptEnginePool {

  private static final Logger LOG = Logger.getLogger(ScriptEnginePool.class);

  private int maxEngines = 0;

  private volatile int activeEngines = 0;

  private HashMap<String, ObjectPool> enginePools;

  private InstanceManager appManager;;

  public ScriptEnginePool(ScriptEngineManager engManager, int maxEngines, Map<String, Integer> initSize,
      InstanceManager appManager) {
    this.maxEngines = maxEngines;
    this.appManager = appManager;
    enginePools = new HashMap<String, ObjectPool>(initSize.size());
    int sum = 0;
    for (String type : initSize.keySet()) {
      int size = initSize.get(type);
      sum += size;
      LOG.info("Initializing " + size + " script engines for " + type + " ...");
      ObjectPool enginePool = new StackObjectPool(new ScriptEngineFactory(engManager, type), size, size);
      try {
        PoolUtils.prefill(enginePool, size);
      }
      catch (Exception ex) {
        LOG.warn("Exception caught while prefilling engine pool of type " + type, ex);
      }
      enginePool = PoolUtils.synchronizedPool(enginePool);
      enginePools.put(type, enginePool);
    }
    if (sum > maxEngines) {
      this.maxEngines = sum;
      LOG.warn("Given maxEngines (" + maxEngines
          + ") is less than the sum of the initial pool sizes.  Using maxEngines = " + sum + " instead.");
    }
    LOG.info("Done with the initialization of script engine pool.");
  }

  public int getMaxEngines() {
    return maxEngines;
  }

  public int getActiveEngines() {
    return activeEngines;
  }

  public Set<String> getEngineTypes() {
    return Collections.unmodifiableSet(enginePools.keySet());
  }

  public int getActiveEngines(String type) {
    int activeEngines = 0;
    ObjectPool enginePool = enginePools.get(type);
    if (enginePool != null) {
      activeEngines = enginePool.getNumActive();
    }
    return activeEngines;
  }

  public int getIdleEngines(String type) {
    int activeEngines = 0;
    ObjectPool enginePool = enginePools.get(type);
    if (enginePool != null) {
      activeEngines = enginePool.getNumIdle();
    }
    return activeEngines;
  }

  public ScriptEngine get(final Application app) {
    String type = app.getScriptType();
    ScriptEngine engine = null;
    if (activeEngines < maxEngines) {
      ObjectPool enginePool = enginePools.get(type);
      if (enginePool != null) {
        try {
          engine = (ScriptEngine) enginePool.borrowObject();
          synchronized (this) {
            ++activeEngines;
          }
          if (LOG.isDebugEnabled()) {
            LOG.debug("Thread " + Thread.currentThread().getName() + " acquired engine " + engine + " of type " + type
                + ", activeEngines = " + activeEngines);
          }
        }
        catch (Exception ex) {
          // Letting method return null. Logging exception in case it is needed.
          LOG.warn("Could not obtain ScriptEngine of type " + type, ex);
        }
      }
    }
    else {
      throw new RuntimeException("Maximum number of engines (" + maxEngines + ") has been reached.");
    }
    return engine;
  }

  public void put(String type, ScriptEngine engine) {
    if (engine != null) {
      ObjectPool enginePool = enginePools.get(type);
      if (enginePool != null) {
        try {
          engine.getBindings(ScriptContext.ENGINE_SCOPE).clear();
          enginePool.returnObject(engine);
        }
        catch (Exception ex) // This also sucks!
        {
          LOG.warn("Could not return ScriptEngine of type " + type + " to the pool (Thread "
              + Thread.currentThread().getName() + ")", ex);
        }
        synchronized (this) {
          --activeEngines;
        }
        if (LOG.isDebugEnabled()) {
          LOG.debug("Thread " + Thread.currentThread().getName() + " returned engine " + engine + " of type " + type
              + ", activeEngines = " + activeEngines);
        }
      }
      else {
        LOG.error(
            "Thread " + Thread.currentThread().getName() + " tried to return a ScriptEngine of unknown type: " + type);
      }
    }
    else {
      LOG.warn(
          "Thread " + Thread.currentThread().getName() + " attempted to return a null ScriptEngine of type " + type);
    }
  }

  private static class ScriptEngineFactory implements PoolableObjectFactory {
    private ScriptEngineManager engineManager;

    private String type;

    private javax.script.ScriptEngineFactory tropoJSEngineFactory;

    ScriptEngineFactory(ScriptEngineManager engineManager, String type) {
      this.engineManager = engineManager;
      this.type = type;
      if (type.equalsIgnoreCase("js")) {
        List<javax.script.ScriptEngineFactory> factories = engineManager.getEngineFactories();

        for (javax.script.ScriptEngineFactory factory : factories) {
          if (factory.getNames().contains("rhino-nonjdk")) {
            LOG.debug("Using rhino-nonjdk as JS engine.");
            tropoJSEngineFactory = factory;
          }
        }
      }
    }

    public Object makeObject() {
      if (LOG.isDebugEnabled()) {
        LOG.debug("Creating " + type + " engine");
      }

      if (type.equalsIgnoreCase("js") && tropoJSEngineFactory != null) {
        return tropoJSEngineFactory.getScriptEngine();
      }

      return engineManager.getEngineByName(type);
    }

    public void activateObject(Object obj) {
    }

    public void destroyObject(Object obj) {
    }

    public void passivateObject(Object obj) {
    }

    public boolean validateObject(Object obj) {
      return true;
    }
  }
}
