package com.tropo.test.driver.scripting.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

public class Utils {

  public static final Map<String, String> ScriptHeaders = new HashMap<String, String>();

  public static final Map<String, String> CommentCharacterMap = new HashMap<String, String>();

  static {
    ScriptHeaders.put("jruby", "com/tropo/test/driver/scripting/ruby/driver.rb");

    CommentCharacterMap.put("jruby", "    #");
  }

  public static String resolveEngineType(final String urlString) {
    return "jruby";
  }

  public static String toFormattedString(Object o) {
    if (o == null) {
      return "";
    }
    String r = null;
    if (o instanceof Throwable) {
      Throwable t = (Throwable) o;
      StringWriter s = new StringWriter();
      PrintWriter p = new PrintWriter(s);
      t.printStackTrace(p);
      r = s.toString();
    }
    if (o instanceof String) {
      r = (String) o;
    }
    else {
      r = o.toString();
    }
    r = r.replaceAll("\r", " ").replaceAll("\n", " ").replaceAll("\t", " ");
    r = r.replaceAll("\\r", " ").replaceAll("\\n", " ").replaceAll("\\t", " ");
    return r;
  }
}
