package com.tropo.test.driver.scripting.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import javax.script.Compilable;
import javax.script.CompiledScript;
import javax.script.ScriptEngine;
import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.micromethod.common.util.StringUtils;
import com.tropo.test.driver.Request;
import com.tropo.test.driver.scripting.Configuration;
import com.tropo.test.driver.scripting.util.Utils;

public class Application {

  private static final Logger LOG = Logger.getLogger(Application.class);

  private String applicationName;
  
  private String scriptURL;

  private String scriptType;

  private Map<String, String> requestParameters;

  private CompiledScript _compiledScript;

  private InstanceManager instanceManager;

  private int shimLineNum;

  public Application(InstanceManager instanceManager, Request request) {
    super();
    String baseURL = request.getParameters().get(Request.SCRIPT_BASE_URL);
    if (StringUtils.isEmpty(baseURL)) {
      baseURL = Configuration.getInstance().getScriptBaseUrl();
    }
    this.instanceManager = instanceManager;
    applicationName = request.getApplicationName();
    scriptURL = baseURL + request.getParameters().get(Request.DRIVER_SCRIPT);
    scriptType = Utils.resolveEngineType(scriptURL);
    requestParameters = request.getParameters();
  }

  public String getScriptURL() {
    return scriptURL;
  }

  public String getScriptType() {
    return scriptType;
  }

  public Map<String, String> getRequestParameters() {
    return requestParameters;
  }

  public String getApplicationName() {
    return applicationName;
  }

  public synchronized CompiledScript getCompiledScript() throws ScriptException, IOException {
    if (_compiledScript == null) {
      _compiledScript = createScript(true);
    }
    else {
      _compiledScript = createScript(false);
    }
    return _compiledScript;
  }

  private CompiledScript createScript(final boolean force) throws ScriptException, IOException {
    ScriptEngine engine = null;
    CompiledScript compiledScript = null;

    engine = instanceManager.getScriptEngine(this);
    if (engine == null) {
      throw new ScriptException("Cannot find a script engine matching " + getScriptURL() + ".");
    }
    else {
      if (LOG.isTraceEnabled()) {
        LOG.debug("Thread " + Thread.currentThread().getName() + " got engine to compile script");
      }
      try {
        String source = null;
        String shimSource = null;

        try {
          shimSource = fetchShimScript();// pre fetch shim script
          source = fetchUserScript(new URL(getScriptURL()), getScriptType(), force);
        }
        catch (Exception e) {
          LOG.error("Exception when fetching script.", e);
          throw new RuntimeException("Exception when fetching script.", e);
        }

        if (engine instanceof Compilable && !(engine.getClass().getSimpleName().equals("JRubyEngine"))) {
          compiledScript = ((Compilable) engine).compile(shimSource.concat("\r\n").concat(source));
        }
        else {
          compiledScript = new SimulatedCompiledScript(instanceManager, source, shimSource, this);
        }
      }
      catch (RuntimeException e) {
        throw e;
      }
      finally {
        if (LOG.isDebugEnabled()) {
          LOG.debug("Thread " + Thread.currentThread().getName() + " returning engine after compiling script");
        }
        instanceManager.putScriptEngine(getScriptType(), engine);
      }
    }
    return compiledScript;
  }

  private String fetchShimScript() {
    String scriptHeader = Utils.ScriptHeaders.get(getScriptType());
    if (scriptHeader != null) {
      ClassLoader classLoader = getClass().getClassLoader();
      BufferedReader r = null;
      try {
        r = new BufferedReader(new InputStreamReader(classLoader.getResourceAsStream(scriptHeader), "UTF-8"));
        String shimScript = readShimScript(r);
        return shimScript;
      }
      catch (Exception e) {
        LOG.error(e.getMessage());
        throw new RuntimeException("Exception when reading shim script.", e);
      }
      finally {
        if (r != null) {
          try {
            r.close();
          }
          catch (IOException e) {
          }
        }
      }
    }
    return null;
  }

  private String fetchUserScript(URL applicationUrl, String type, boolean force) throws IOException {
    String retval = null;

    if (applicationUrl != null) {
      InputStream urlStream = null;
      try {
        HttpURLConnection connection = (HttpURLConnection) applicationUrl.openConnection();
        urlStream = connection.getInputStream();
        BufferedReader r = new BufferedReader(new InputStreamReader(urlStream, "UTF-8"));
        retval = readUserScript(r);
      }
      catch (IOException ioe) {
        LOG.error("Failed to load script at " + applicationUrl, ioe);
        throw new IOException("Failed to load script at " + applicationUrl);
      }
      finally {
        if (urlStream != null)
          IOUtils.closeQuietly(urlStream);
      }
    }
    return retval;
  }

  private String readShimScript(BufferedReader r) throws IOException {
    final StringBuffer b = new StringBuffer();

    int lineNo = 0;
    try {
      String l = r.readLine();
      while (l != null) {
        ++lineNo;
        b.append(l.concat("\r\n"));
        l = r.readLine();
      }
    }
    catch (RuntimeException e) {
      throw e;
    }
    finally {
      r.close();
    }
    shimLineNum = lineNo;
    return b.toString();
  }

  private String readUserScript(BufferedReader r) throws IOException {
    final StringBuffer b = new StringBuffer();

    long lineNo = shimLineNum;
    try {
      String l = r.readLine();
      while (l != null) {
        ++lineNo;
        b.append(l.concat("\r\n"));
        LOG.debug(
            l.concat(Utils.CommentCharacterMap.get(getScriptType())).concat(" line ").concat(String.valueOf(lineNo)));
        l = r.readLine();
      }
    }
    catch (IOException e) {
      throw e;
    }
    finally {
      r.close();
    }
    return b.toString();
  }
}
