package com.tropo.test.driver.scripting.impl;

import java.io.File;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.script.Compilable;
import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;
import javax.script.ScriptEngineManager;

import org.apache.log4j.Logger;

import com.tropo.test.driver.Instance;
import com.tropo.test.driver.Request;

public class InstanceManager {

  private static final Logger LOG = Logger.getLogger(InstanceManager.class);

  private ScriptEngineManager scriptEngineManager = new ScriptEngineManager();

  private String jrubyHome = "/opt/voxeo/prism/server/apps/driver/jruby";

  private ScriptEnginePool _enginePool;

  private Map<String, InstanceImpl> applicationInstances = new ConcurrentHashMap<String, InstanceImpl>();

  public InstanceManager() {
    super();
    rubyHome();
    scriptEngineManager = new ScriptEngineManager();
    LOG.info("Initializing Script Engine Manager [" + scriptEngineManager + "] ...");

    for (final ScriptEngineFactory f : scriptEngineManager.getEngineFactories()) {
      final ScriptEngine e = f.getScriptEngine();
      final StringBuffer log = new StringBuffer("Engine=" + f.getEngineName() + "[" + f.getEngineVersion() + "], lang="
          + f.getLanguageName() + "[" + f.getLanguageVersion() + "], alias=" + f.getNames());
      log.append(e instanceof Compilable ? ", compilable=true" : ", compiable=false");
      log.append(e instanceof Invocable ? ", invocable=true" : ", invocable=false");
      log.append(e instanceof Serializable ? ", serializable=true" : ", serializable=false.");
      LOG.info(log.toString());
    }

    Map<String, Integer> engineSizes = new HashMap<String, Integer>();
    engineSizes.put("jruby", 100);
    _enginePool = new ScriptEnginePool(scriptEngineManager, 100, engineSizes, this);
  }

  public String rubyHome() {
    if (!new File(jrubyHome).isDirectory()) {
      throw new RuntimeException(jrubyHome + " the jrubyHome directory does not exist.");
    }

    System.setProperty("jruby.home", jrubyHome);
    System.setProperty("com.sun.script.jruby.loadpath", jrubyHome);
    System.setProperty("com.sun.script.jruby.terminator", "on");
    LOG.info("jruby.home is " + jrubyHome);
    return jrubyHome;
  }

  public Instance createInstance(Request request) {
    Application application = new Application(this, request);
    return new InstanceImpl(application, this);
  }

  public ScriptEngine getScriptEngine(Application app) {
    return _enginePool.get(app);
  }

  public void putScriptEngine(String type, ScriptEngine engine) {
    _enginePool.put(type, engine);
  }

  public void removeApplicationInstance(InstanceImpl instance) {
    applicationInstances.remove(instance.getInstanceId());
  }

  public void addApplicationInstance(InstanceImpl instance) {
    this.applicationInstances.put(instance.getInstanceId(), instance);
  }
}
