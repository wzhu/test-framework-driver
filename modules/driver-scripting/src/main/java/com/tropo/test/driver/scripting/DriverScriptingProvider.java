package com.tropo.test.driver.scripting;

import com.tropo.test.driver.Driver;
import com.tropo.test.driver.Instance;
import com.tropo.test.driver.Request;
import com.tropo.test.driver.scripting.impl.InstanceManager;

public class DriverScriptingProvider implements Driver {

  private static final String NAME = "scriptingdriver";

  private InstanceManager instanceManager;

  public DriverScriptingProvider() {
    instanceManager = new InstanceManager();
  }

  @Override
  public Instance getInstance(Request request) {
    if (!getName().equalsIgnoreCase(request.getDriverName())) {
      return null;
    }

    return instanceManager.createInstance(request);
  }

  @Override
  public String getName() {
    return NAME;
  }
}
