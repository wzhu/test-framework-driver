package com.tropo.test.driver.scripting.impl;

import java.lang.reflect.Field;
import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.WeakHashMap;
import java.util.concurrent.Future;

import javax.script.CompiledScript;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptException;
import javax.script.SimpleScriptContext;

import org.apache.log4j.Logger;

import com.micromethod.common.util.logging.SLogger;
import com.tropo.test.driver.Instance;
import com.tropo.test.driver.Locator;
import com.tropo.test.driver.Result;
import com.tropo.test.driver.api.Call;
import com.tropo.test.driver.impl.ResultImpl;
import com.tropo.test.driver.scripting.ScriptingInstanceAPI;
import com.tropo.test.driver.scripting.util.SettableResultFuture;
import com.tropo.test.driver.scripting.util.Utils;

// TODO sip/application session/ log context
public class InstanceImpl implements Instance, ScriptingInstanceAPI {

  private static final Logger LOG = Logger.getLogger(InstanceImpl.class);

  private InstanceManager instanceManager;

  private Application application;

  private String instanceId;

  private ScriptContext _context;

  private Throwable scriptThrowable = null;

  private SessionThreadGroup threadGroup;

  private WeakHashMap<Thread, Object> spawnedThreads = new WeakHashMap<Thread, Object>();

  private InstanceCallManager instanceCallManager;

  public InstanceImpl(Application app, InstanceManager manager) {
    super();
    instanceId = UUID.randomUUID().toString();
    application = app;
    instanceManager = manager;
    instanceCallManager = new InstanceCallManager(this, Locator.getCallManager());
    threadGroup = new SessionThreadGroup(Thread.currentThread().getThreadGroup(), this, instanceId);
    createScriptContext();
  }

  protected ScriptContext createScriptContext() {
    _context = new SimpleScriptContext();
    _context.setAttribute("instance", this, ScriptContext.ENGINE_SCOPE);
    _context.setAttribute("application", application, ScriptContext.ENGINE_SCOPE);
    _context.setAttribute("callManager", instanceCallManager, ScriptContext.ENGINE_SCOPE);
    Map<String, String> parameters = application.getRequestParameters();
    if (parameters != null) {
      for (final Object name : application.getRequestParameters().keySet()) {
        _context.setAttribute((String) name, parameters.get(name), ScriptContext.ENGINE_SCOPE);
        if (LOG.isDebugEnabled()) {
          LOG.debug(
              name + ":" + parameters.get(name) + " is added into the context  of app instance : " + this.toString());
        }
      }
    }
    return _context;
  }

  @Override
  public Future<Result> start() {
    final String sessionId = getInstanceId();
    final SettableResultFuture<Result> future = new SettableResultFuture<Result>();

    try {
      Thread sessionThread = new Thread(getThreadGroup(), new Runnable() {
        private Map<String, String> context = SLogger.getCurrentContexts();

        public void run() {
          SLogger.inheritContexts(context);
          instanceManager.addApplicationInstance(InstanceImpl.this);
          try {
            evaluateScript();
          }
          finally {
            instanceManager.removeApplicationInstance(InstanceImpl.this);
            LOG.warn("Instance " + sessionId + " removed");
            SLogger.clearContexts();
          }
          Result result = null;
          if (scriptThrowable != null) {
            result = new ResultImpl(500, scriptThrowable, null);
          }
          else {
            result = new ResultImpl(200, scriptThrowable, null);
          }
          future.setResult(result);
        }
      }, sessionId);

      sessionThread.setDaemon(true);
      sessionThread.setName("Driver-Thread-" + sessionId);

      sessionThread.start();
    }
    catch (Exception ex) {
      LOG.warn("Exception before executing instance:" + this, ex);
    }

    return future;
  }

  protected void evaluateScript() {
    ScriptEngine eng = null;
    ScriptEngine oldEng = null;
    CompiledScript compiledScript = null;
    try {
      compiledScript = application.getCompiledScript();
    }
    catch (Exception e) {
      throw new RuntimeException("Exception when get compliled script.", e);
    }
    try {
      if (LOG.isDebugEnabled()) {
        LOG.debug(this + " starts execution on Thread " + Thread.currentThread().getName());
      }

      if (compiledScript instanceof SimulatedCompiledScript) {
        eng = application.getCompiledScript().getEngine();
      }
      else {
        eng = instanceManager.getScriptEngine(application);
        oldEng = replaceScriptEngine(application.getCompiledScript(), eng);
      }
      compiledScript.eval(_context);
      if (LOG.isDebugEnabled()) {
        LOG.debug(this + " ends execution on Thread " + Thread.currentThread().getName());
      }
    }
    catch (final ScriptException e) {
      scriptThrowable = e;
      LOG.error("Exception when executing script", e);
    }
    catch (final SecurityException e) {
      scriptThrowable = e;
      LOG.error(this + " violates the sandbox: " + e.getMessage(), e);
    }
    catch (final Throwable t) {
      scriptThrowable = t;
      if (t instanceof UnknownHostException) {
        LOG.error(this + " has unknown host error: " + t.getMessage());
      }
      else {
        LOG.error(this + " has unknown errors: " + t.getMessage(), t);
      }
    }
    finally {
      _context.getBindings(ScriptContext.ENGINE_SCOPE).clear();
      if (eng != null) {
        if (oldEng != null && eng != oldEng) {
          replaceScriptEngine(compiledScript, oldEng);
        }
        instanceManager.putScriptEngine(application.getScriptType(), eng);
      }
      instanceCallManager.hangupcalls();
    }
  }

  private Field getField(Object o, String name) throws SecurityException {
    try {
      return o.getClass().getDeclaredField(name);
    }
    catch (NoSuchFieldException e) {
      ; // ignore
    }
    return null;
  }

  private ScriptEngine replaceScriptEngine(CompiledScript cs, ScriptEngine eng) {
    if (cs instanceof SimulatedCompiledScript || cs == null) {
      return eng;
    }
    String fn = "engine";
    ScriptEngine oldEng = cs.getEngine();
    try {
      Field f = getField(cs, fn); // groovy, js
      if (f == null) {
        fn = "_engine";
        f = getField(cs, fn); // php

      }
      if (f == null) {
        fn = "this$0";
        f = getField(cs, fn); // jython, jruby
      }

      if (f == null) {
        Field[] fs = cs.getClass().getDeclaredFields();
        StringBuffer buf = new StringBuffer("All available fields in " + cs.getClass().getName() + " are: ");
        for (int i = 0; i < fs.length; i++) {
          if (i == fs.length - 1) {
            buf.append('[').append(fs[i].getName()).append(']');
          }
          else {
            buf.append('[').append(fs[i].getName()).append(']').append(",");
          }
        }
        LOG.error(toString() + " no field with name 'engine' or '_engine' . " + buf.toString());
        return eng;
      }
      final boolean accessibility = f.isAccessible();
      f.setAccessible(true);
      f.set(cs, eng);
      f.setAccessible(accessibility);
      if (LOG.isTraceEnabled()) {
        LOG.trace(
            toString() + " replaced script engine [" + oldEng + "] --> [" + cs.getEngine() + "] in field <" + fn + ">");
      }
      return oldEng;
    }
    catch (SecurityException e) {
      if (LOG.isDebugEnabled()) {
        LOG.debug(toString() + " error replacing script engine : " + e.getMessage(), e);
      }
    }
    catch (IllegalArgumentException e) {
      if (LOG.isDebugEnabled()) {
        LOG.debug(toString() + " error replacing script engine : " + e.getMessage(), e);
      }
    }
    catch (IllegalAccessException e) {
      if (LOG.isDebugEnabled()) {
        LOG.debug(toString() + " error replacing script engine : " + e.getMessage(), e);
      }
    }
    return eng;
  }

  public String getInstanceId() {
    return instanceId;
  }

  public void threadSpawned(Thread thread) {
    synchronized (spawnedThreads) {
      spawnedThreads.put(thread, null);
    }
  }

  public Set<Thread> getThreads() {
    return new HashSet<Thread>(spawnedThreads.keySet());
  }

  public ThreadGroup getThreadGroup() {
    return threadGroup;
  }

  public Application getApplication() {
    return application;
  }

  @Override
  public void log(Object msg) {
    if (msg instanceof Throwable) {
      final Throwable t = (Throwable) msg;
      LOG.error("###SCRIPTING LOG:" + Utils.toFormattedString(t));
      LOG.error("", t);
    }
    else {
      LOG.info("###SCRIPTING LOG:" + Utils.toFormattedString(msg));
    }
  }

  public synchronized void block(final long milliSeconds) {
    final long begin = System.currentTimeMillis();
    long time = milliSeconds;
    while (time > 0) {
      try {
        wait(time);
      }
      catch (final InterruptedException e) {
        ;// ignore
      }
      time = milliSeconds - (System.currentTimeMillis() - begin);
    }
  }

  @Override
  public void waitForReadyNotify() {
    // TODO Auto-generated method stub

  }

  @Override
  public void nofityReady() {
    // TODO Auto-generated method stub

  }

  public void askWithCallBack(String grammar, int timeout, Call call) {

  }
}
