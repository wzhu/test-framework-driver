package com.tropo.test.driver.scripting;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class Configuration {
  private static final Logger LOG = Logger.getLogger(Configuration.class);

  public static Configuration instance = new Configuration();

  private Properties properties;

  public static Configuration getInstance() {
    return instance;
  }

  private Configuration() {
    properties = new Properties();
    InputStream stream = Configuration.class.getClassLoader().getResourceAsStream("driver.properties");
    try {
      properties.load(stream);
    }
    catch (IOException e) {
      LOG.warn("Exception when loading configuration.", e);
    }
  }

  public String getScriptBaseUrl() {
    return properties.getProperty("ScriptBaseURL");
  }
}
