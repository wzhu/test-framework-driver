package com.tropo.test.driver.scripting.impl;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import com.tropo.test.driver.api.Call;
import com.tropo.test.driver.api.CallManager;
import com.tropo.test.driver.api.IncomingCall;
import com.tropo.test.driver.api.IncomingCallListener;
import com.tropo.test.driver.api.OutgoingCall;

public class InstanceCallManager implements CallManager, IncomingCallListener {

  private Map<String, Call> calls = new ConcurrentHashMap<String, Call>();

  private InstanceImpl instance;

  private CallManager callManager;

  private BlockingQueue<IncomingCall> incomingCallQueue = new LinkedBlockingQueue<IncomingCall>(10);

  protected List<IncomingCallListener> incomingCallListeners = new LinkedList<IncomingCallListener>();

  public InstanceCallManager(InstanceImpl instance, CallManager callManager) {
    super();
    this.instance = instance;
    this.callManager = callManager;
    callManager.addIncomingCallLisener(this);
  }

  @Override
  public synchronized OutgoingCall createOutgoingcall(String to, String from, boolean inviteTakeSDP,
      Map<String, String> headers) {
    OutgoingCall call = callManager.createOutgoingcall(from, to, inviteTakeSDP, headers);
    calls.put(call.getID(), call);
    return call;
  }

  @Override
  public synchronized OutgoingCall createOutgoingcall(String to, String from, boolean inviteTakeSDP,
      Map<String, String> headers, String requestUri) {
    OutgoingCall call = callManager.createOutgoingcall(from, to, inviteTakeSDP, headers, requestUri);
    calls.put(call.getID(), call);
    return call;
  }

  @Override
  public synchronized Collection<Call> getCalls() {
    return calls.values();
  }

  @Override
  public IncomingCall waitForIncomingCall() throws InterruptedException {
    return incomingCallQueue.poll(30, TimeUnit.SECONDS);
  }

  @Override
  public void addIncomingCallLisener(IncomingCallListener listener) {
    incomingCallListeners.add(listener);
  }

  @Override
  public void removeIncomingCallLisener(IncomingCallListener listener) {
    incomingCallListeners.remove(listener);
  }

  public synchronized void hangupcalls() {
    for (Call call : calls.values()) {
      call.hangup(null);
    }
  }

  @Override
  public synchronized void onIncomingCall(IncomingCall call) {
    if (call.getCallee().equalsIgnoreCase(instance.getApplication().getApplicationName())) {
      calls.put(call.getID(), call);
      incomingCallQueue.offer(call);
    }
  }
}
