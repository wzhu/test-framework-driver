package com.tropo.test.driver.scripting;

public interface ScriptingInstanceAPI {

  String getInstanceId();

  void log(Object message);

  void block(long ms);

  void waitForReadyNotify();

  void nofityReady();
}
