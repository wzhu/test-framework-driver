# encoding: UTF-8
require 'com/tropo/test/driver/scripting/ruby/driver_common'

$appInstance = TropoTestDriver::AppInstance.new($instance)

def createOutboundCall(to, options={})
  default_options = {
    :callerID => "sip:testdriver@tropo.cisco.com",
    :headers       => nil,
    :inviteTakeSDP => true,
    :requestUri => nil
  }

  localOptions = options.clone
  localOptions = default_options.merge(localOptions.key_strings_to_symbols!)

  _newCall_ = $callManager.createCall(to,
  localOptions[:callerID],
  localOptions[:headers],
  localOptions[:inviteTakeSDP],
  localOptions[:requestUri])
  _call_ = Tropo::TropoOutboundCall.new(_newCall_)
end

def waitIncomingCall()
  $callManager.waitForIncomingCall();
end

def wait(milliSeconds=0)
  return $instance.block(milliSeconds)
end

def log(msg=nil)
  $instance.log(msg)
end

def waitForReadyNotify
  $instance.waitForReadyNotify()
end

def nofityReady
  $instance.nofityReady()
end