module TropoTestDriver
  class AppInstance
    attr_reader :instance
    def initialize(instance)
      @instance = instance
    end

    def instanceID
      instance.getInstanceId()
    end
  end

  class TropoCall
    attr_reader :call
    def initialize(call)
      @call = call
    end

    def id()
      return call.getID()
    end

    def state()
      return call.getState().toString()
    end

    def caller()
      return call.getCaller()
    end

    def callee()
      return call.getCallee()
    end

    def say( text)
      call.say(text)
    end

    def sayDTMF(dtmf)
      call.sayDTMF(dtmf)
    end

    def ask(grammar, options = {})
      default_options = {
        :onReady => nil,
        :timeout => 30
      }

      localOptions = options.clone
      localOptions = default_options.merge(localOptions.key_strings_to_symbols!)
      #if localOptions[:onReady]
      call.ask(grammar, localOptions[:timeout], localOptions[:onReady])
    end

    #    SipServletRequest getReceivedReInvite();
    #    SipServletRequest getOriginalRequest();
    #    byte[] getLocalSDP();
    #    byte[] getRemoteSDP();
  end

  class TropoIncomingCall < TropoCall
    def initialize(call)
      super(call)
    end

    def sendProvisiningResponse(code=180, options={})
      default_options = {
        :withSDP => false,
        :isReliable => false,
        :headers => nil,
        :content => nil,
        :contentType => nil,
        :createNewNc => false}
      localOptions = options.clone
      localOptions = default_options.merge(localOptions.key_strings_to_symbols!)

      call.sendProvisiningResponse(code,localOptions[:withSDP], localOptions[:isReliable], localOptions[:headers], localOptions[:content], localOptions[:contentType], localOptions[:createNewNc])
    end

    def acceptEarlyMedia(code=183, headers={})
      call.acceptEarlyMedia(code, headers)
    end

    def answer(headers={})
      call.answer(headers)
    end

    def reject(code=603, headers={})
      call.reject(code, headers)
    end
  end

  class TropoOutboundCall < TropoCall
    def initialize(call)
      super(call)
    end

    def waitEarlyMedia()
      call.waitEarlyMedia()
    end

    def waitAnswer()
      call.waitAnswer()
    end

    #SipServletResponse waitForProvisiningResponse(int timeout) throws InterruptedException;
  end

end